//David Parker 2139469
package linearalgebra;

public class Vector3d {
    //fields
    private double x;
    private double y;
    private double z;

    //constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //getters
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    //custom methods
    public double magnitude() {
        final int exponent = 2;
        return Math.sqrt(Math.pow(this.x, exponent) + Math.pow(this.y, exponent) + Math.pow(this.z, exponent));
    }

    public double dotProduct(Vector3d vector) {
        return this.x * vector.getX() + this.y * vector.getY() + this.z * vector.getZ();
    }

    public Vector3d add(Vector3d vector) {
        double newX = this.x + vector.getX();
        double newY = this.y + vector.getY();
        double newZ = this.z + vector.getZ();
        return new Vector3d(newX, newY, newZ);
    }
}
