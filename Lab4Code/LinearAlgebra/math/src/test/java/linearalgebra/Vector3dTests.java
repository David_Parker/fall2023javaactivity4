//David Parker 2139469
package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    @Test
    public void getMethods_returnsVectorsCreated() {
        Vector3d vector = new Vector3d(2, 3, 5);
        assertEquals("Expect x", 2, vector.getX(), 0);
        assertEquals("Expect y", 3, vector.getY(), 0);
        assertEquals("Expect z", 5, vector.getZ(), 0);
    }

    @Test
    public void magnitude_returnsMagnitudeOfVector() {
        Vector3d vector = new Vector3d(2, 3, 5);
        assertEquals("Expected the magnitude of the vector", 6.16,vector.magnitude(), 0.01);
    }

    @Test
    public void dotProduct_returnsDotProductOfTwoVectors() {
        Vector3d vector1 = new Vector3d(2, 3, 5);
        Vector3d vector2 = new Vector3d(7, 11, 13);
        assertEquals("Expected the dot product of 2 vectors", 112, vector1.dotProduct(vector2), 0);
    }

    @Test
    public void add_returnsAdditionOfTwoVectors() {
        Vector3d vector1 = new Vector3d(2,3,5);
        Vector3d vector2 = new Vector3d(7, 11, 13);
        Vector3d testVector = vector1.add(vector2);
        assertEquals("Expected the addition of both Xs", 9, testVector.getX(), 0);
        assertEquals("Expected the addition of both Ys", 14, testVector.getY(), 0);
        assertEquals("Expected the addition of both Zs", 18, testVector.getZ(), 0);
    }
}
